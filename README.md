# censor

Searches a file tree and replaces certain text which might appear in text file contents, file names, or directory names.

For example, you place a list of words which should not appear in a file tree into censor.txt. Then, simply run "censor.py censor.txt path-to-file-tree  out"

The code will then do the following:

 * Copy the entire tree from path-to-file-tree into a new folder named "out".
 * Iterate through every text file in "out". Replace every censored word with xx123xx where 123 is a unique number for each word that is replaced.
 * Iterate through every file name and directory name in the tree. Rename any filenames/directory names which contain the censored word.

Note that any occurance of the censored text will be replaced---even when it appears as a substring in another word.

 ## Bugs, Gotchas

Since this program edits files, filenames, and directory names, a bug could potentially cause serious problems to a directory tree. Use it at your own risk. 

Words in the censored word list are tokenized based on whitespace, '@' (to tokenize out usernames from email addresses) and ',' and '"' (to aid in parsing CSV files).

If a word in the censored word list is only one character, it is ignored.

We do not provide a program to undo the censoring.

The code will read the entire text files into memory before trying to replace the text in the file.

The program only censors files which appear to be ASCII or UTF-8 text. Binary files are not censored.


#!/usr/bin/env python3
# Author: Scott Kuhl

import random, re, shutil, os

def load_censored_words(filename):
    """Loads a list of words from a file. Ignores words which are only one character. Words can be separated by newlines, spaces, tabs or @"""

    content = ""
    with open(filename, 'r') as f:
        content = f.read()

    import re
    words = re.split("[\r\n \t@,\"]", content)

    # remove words that are a single character or 0 characters:
    censored_words = [ x for x in words if len(x) > 1 ]

    # make a dictionary. Key=string to search for; value=replacement text
    replacementDict = {}
    count = 1
    for cw in censored_words:
        # Store all words to be replaced in lowercase. We will ignore case when searching for the strings.
        replacementDict[cw.lower()] = "xx"+str(count)+"xx"
        count = count+1
    
    # We return a list that may have duplicates in it if there were
    # duplicates in the censor.txt file.
    return replacementDict


def multireplace(string, replacements):
    """
    Code from: https://stackoverflow.com/a/36620263/474549

    Given a string and a replacement map, it returns the replaced string. Only makes
    one pass through the file instead of multiple passes.

    :param str string: string to execute replacements on
    :param dict replacements: replacement dictionary {value to find: value to replace}
    :rtype: (str, int)

    """
    # Place longer ones first to keep shorter substrings from matching
    # where the longer ones should take place
    # For instance given the replacements {'ab': 'AB', 'abc': 'ABC'} against 
    # the string 'hey abc', it should produce 'hey ABC' and not 'hey ABc'
    substrs = sorted(replacements, key=len, reverse=True)

    # Create a big OR regex that matches any of the substrings to replace
    regexp = re.compile('|'.join(map(re.escape, substrs)), flags=re.IGNORECASE)

    # For each match, look up the new string in the
    # replacements.
    return regexp.subn(lambda match: replacements[match.group(0).lower()], string)
    


def replace_string(text, censorDict):
    """Same as multireplace, but returns None when the text is unchanged or returns the new text if the text has changed"""
    (newtext, replaceCount) = multireplace(text, censorDict)
    if newtext == text:
        return None
    return newtext


def replace_file(filename, censorDict):
    """Replace all occurrences of censored words with xx123xx where 123
    means that it is the 123rd word in the censored word list."""

    # read file content into string
    content = ""
    try:
        with open(filename, "r") as f:
            content = str(f.read())
    except UnicodeDecodeError:
        print("%s: Skipping replacement, does not appear to be text file." % filename)
        return


    (content, totalReplacedCount) = multireplace(content, censorDict)

    # # replace all of the words, count number of replacements, report number of replacements
    # totalReplacedCount = 0
    # for orig, replacement in censorDict:
    #     (content, replaceCount) = re.subn(orig, replacement, content, flags=re.I)
    #     if replaceCount > 0:
    #         print("%s: %s -> %s (%d times)" % (filename,orig,replacement,replaceCount))
    #     totalReplacedCount = totalReplacedCount+replaceCount

    # print(content)

    # write modified file back in place
    if totalReplacedCount > 0:
        with open(filename, "w") as f:
            f.write(content)

    print("%s: %d replacements" % (filename, totalReplacedCount))


    
def censor_filename(top, fname, censorDict):
    """Given a directory and a filename, rename the file as needed"""
    new_fname=replace_string(fname, censorDict)
    if new_fname:
        old_path = os.path.join(top, fname)
        new_path = os.path.join(top, new_fname)
        if os.path.exists(new_path):
            print("Error: Unable to rename file %s to %s because destination already exists." % (old_path, new_path))
            exit(1)
        else:
            print("Renaming file %s to %s." % (old_path, new_path))
            os.rename(old_path, new_path)

def censor_dirname(top, dname, censorDict):
    """Given a directory (and a subdirectory), rename the subdirectory as needed."""
    new_dname = replace_string(dname, censorDict)
    if new_dname:
        old_path = os.path.join(top, dname)
        new_path = os.path.join(top, new_dname)
        if os.path.exists(new_path):
            print("Error: Unable to rename directory %s to %s because destination already exists." % (old_path, new_path))
            exit(1)
        else:
            print("Renaming directory %s to %s." % (old_path, new_path))
            os.rename(old_path, new_path)
    

import sys
if len(sys.argv) != 4:
    print("Usage: %s listOfCensoredWords.txt pathToCensor outputPath" % sys.argv[0])
    exit(1)
argCensorFile = sys.argv[1]
argInputPath = sys.argv[2]
argOutputPath = sys.argv[3]

if argInputPath == argOutputPath:
    print("The pathToCensor '%s' and outputPath '%s' must be different." % (argInputPath, argOutputPath))

if os.path.exists(argOutputPath):
    print("Path '%s' already exists. Delete it or use a new destination name to continue." % argOutputPath)
    exit(1)

    
# load words to censor
c = load_censored_words(argCensorFile)

# Copy tree into new location where we can do replacements
shutil.copytree(argInputPath, argOutputPath)
for (top,dirs,files) in os.walk(argOutputPath, topdown=False):
    for fname in files:
        path=os.path.join(top,fname)
        #print("%s: Processing..." % path)
        
        # censor file contents
        replace_file(path, c)

        # censor the filename itself
        censor_filename(top, fname, c)

    for dname in dirs:
        # censor directory name
        censor_dirname(top, dname, c)


# At end, print a listing of the replacements that occurred:
print()
print()
print("List of replaced words:")
for k,v in c.items():
    print("%s -> %s" % (k,v))

